module.exports = function(grunt) {

  // show elapsed time at the end
  require('time-grunt')(grunt);
  // load all grunt tasks
  require('load-grunt-tasks')(grunt);
  
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
	less: {
		dev: {
			files : [
			{
				expand : true,
				cwd : "src/",
				src : "**/*.less",
				dest: "src/",
				ext: ".css",
				dotExt: "first"
			}
			]
		}
	}
  });
  
  grunt.registerTask('default', ['less']);

};
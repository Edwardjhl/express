环境：
1. node >= 0.12
2. npm 在 node 安装后自动安装
3. mongodb

初期环境搭建：(以下命令不需要在开发环境搭建时输入)
> npm install -g yo
> npm install -g bower
> npm install -g express
> npm install -g mongoose
> npm install -g express-generator@4
> npm install -g generator-express
> yo express
> bower install backbone --save
> bower install jquery#1.11.3 --save
> bower install lodash --save
> bower install bootstrap --save

项目初始化时（搭建开发环境时需要以下命令）
1. > npm install    ## 安装 node modules
2. > bower install  ## 安装 javascript library
3. > grunt          ## 启动 grunt 服务
   -- 请注意 package.json -- 
   "scripts": {
     "start": "node ./bin/www" ## grunt 的时候自动启动
   }
###################### 启动 MongoDB ######################
1. > cd D:\Share\Mongodb\data\db
2. > mongod -dbpath .

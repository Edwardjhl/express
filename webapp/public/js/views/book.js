/**
 * Created by EdwardLee on 2015/7/3.
 */
var app = app || {};

app.BookView = Backbone.View.extend({
    tagName   : "div",
	
    className : "bookContainer",
	
    template  : _.template($("#bookTemplate").html()),

    render : function() {
        this.$el.html(this.template(this.model.toJSON()));

        return this;
    },

    events : {
        "click .delete" : "deleteBook"
    },

    deleteBook : function( item ) {
        this.model.destroy();

        this.remove();
    }
});

/**
 * Created by EdwardLee on 2015/7/3.
 */
var app = app || {};

app.LibraryView = Backbone.View.extend({

    el : "#books",

    events : {
        "click #add" : "addBook"
    },

    initialize : function( initialBooks ) {
        this.collection = new app.Library( initialBooks );

        this.collection.fetch({reset : true});

        this.render();

        this.listenTo(this.collection, "add", this.renderBook);
        this.listenTo(this.collection, "reset", this.render);

    },

    render : function() {
        this.collection.each(function(item) {
            this.renderBook( item );
        }, this);

        return this;
    },

    renderBook : function( item ) {
        var bookView = new app.BookView({
            model : item
        });
        this.$el.append( bookView.render().el );
    },

    addBook: function( e ) {
        e.preventDefault();

        var formData = {};

        $( '#addBook input' ).each( function( i, el ) {

          var $el = $( el),
              val = $el.val();
          if (val) {
            switch (el.id) {
              case "releaseDate" :

                var year = parseInt(val.substring(0, 4), 10),
                   month = parseInt(val.substring(4, 6), 10) - 1,
                    date = parseInt(val.substring(6, 8), 10);

                formData[el.id] = new Date(year, month, date).getTime();

                break;
              case "keywords" :
                var keywords = $el.val().split(/\s+/);

                formData[el.id] = formData[el.id] || [];

                for (var i = 0; i < keywords.length; i++) {
                  formData[el.id].push({ keyword: keywords[i] });
                }
                break;
              default :
                formData[el.id] = $el.val();
                break;
            }
          }
        });
        //this.collection.add( new app.Book( formData ) );
        this.collection.create( formData );
    }
});

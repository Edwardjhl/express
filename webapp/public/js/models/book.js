/**
 * Created by EdwardLee on 2015/7/3.
 */
var app = app || {};

app.Book = Backbone.Model.extend({
   urlRoot: "/api/books",

   defaults: {
       coverImage  : "img/placeholder.png",
       title       : "N/A",
       releaseDate : "N/A",
       keywords    : "N/A"
   },
   // 所有response 属性将会 赋值(copy) 到 app.Book Model
   parse: function( response ) {
     // 便于 /api/books/:id 的路由绑定
     response.id = response._id;
     // 测试 test 属性, app.Book.attributes 将会新增该属性, 详细请查看 Model.fetch 代码
     // response.test = "test";
     // 记得返回 response 对象
     return response;
   }
});

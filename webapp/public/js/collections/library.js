/**
 * Created by EdwardLee on 2015/7/3.
 */
var app = app || {};

app.Library = Backbone.Collection.extend({
    url   : "/api/books",
    model : app.Book
});

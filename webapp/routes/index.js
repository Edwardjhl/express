var express  = require('express');
var router   = express.Router();
var mongoose = require('mongoose');
/* GET home page. */

router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});

router.get('/library', function(req, res) {
  res.render('library', {});
});

var Keywords = new mongoose.Schema({
  keyword : String
});

//Schemas
var Book = new mongoose.Schema({
    title: String,
    author: String,
    releaseDate: Date,
    keywords : [Keywords]
});

//Models
var BookModel = mongoose.model( 'Book', Book );
// find All
router.get( '/api/books', function( request, response ) {

    BookModel.find( function( err, books ) {
        if( !err ) {
            return response.jsonp( books );
        } else {
            return console.log( err );
        }
    });
});
// find by id
router.get( '/api/books/:id', function( request, response ) {

    BookModel.findById( request.params.id, function( err, book ) {
        if( !err ) {
            return response.jsonp( book );
        } else {
            return console.log( err );
        }
    });
});
// insert
router.post( '/api/books', function( request, response ) {

  var book = new BookModel({
    title       : request.body.title,
    author      : request.body.author,
    releaseDate : request.body.releaseDate,
    keywords    : request.body.keywords
  });

  book.save(function(err){
    if(!err) {
      return console.log('created');
    } else {
      return console.log(err);
    }
  });

  return response.send(book);
});
// update
router.put( '/api/books/:id', function( request, response ) {

  BookModel.findById( request.params.id, function( err, book ) {

    book.title       = request.body.title;
    book.author      = request.body.author;
    book.releaseDate = request.body.releaseDate;
    book.keywords    = request.body.keywords;

    book.save(function(err){
      if( !err ) {
        console.log('book updated');
      } else {
        console.log( err );
      }
      return response.jsonp( book );
    })

  });
});
// delete
router.delete( '/api/books/:id', function( request, response ) {

    BookModel.findById(request.params.id, function(err, book) {
      book.remove(function(err) {
        if (!err) {
          console.log("removed");
          return response.send("success");
        } else {
          console.log(err);
        }
      });
    });
});

module.exports = router;

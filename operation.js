$.get("/api/books", function (data) {
    console.log(data);
});
$.get("/api/books/55a1b76baffeba5c0586c5c0", function (data) {
    console.log(data);
});
$.post("/api/books/", {
    title: "JavaScript",
    author: 'LeeJan',
    releaseDate: new Date(2015, 4, 1).getTime()
}, function (data, status) {
    console.dir(data);
    console.log(status);
});
$.ajax({
    url: "/api/books/55a1b76baffeba5c0586c5c0",
    type: "PUT",
    data: {
        title: "JavaScript Action",
        author: 'Apple',
        releaseDate: new Date(2015, 4, 1).getTime()
    },
    success: function (data, status) {
        console.dir(data);
        console.log(status);
    }
});
$.ajax({
    url: "/api/books/55a1b76baffeba5c0586c5c0",
    type: "DELETE",
    success: function (data, status) {
        console.dir(data);
        console.log(status);
    }
});
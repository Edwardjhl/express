module.exports = function (grunt) {
    'use strict';

    var lessPlugins = [
        new (require('less-plugin-autoprefix'))({
            browsers : ["Chrome >= 20", "Firefox >= 24", "Explorer >= 8", "Safari >= 6"]
        })
    ];


    grunt.initConfig({
        /* metadata */
        pkg   : grunt.file.readJSON('package.json'),
        paths : {
            build            : "dist/dev/coremail.grunt",
            components_build : '<%= paths.build %>/framework/@project.revision@/components',
            XT5              : {
                src         : 'web.modules/coremail.XT5/src',
                asset_src   : '<%= paths.XT5.src %>/XT5/@project.revision@',
                asset_build : '<%= paths.build %>/XT5/@project.revision@'
            }
        },

        /* task */
        clean : {
            build         : ['<%= paths.build %>'],
            XT5           : ['<%= paths.build %>/{XT5,WEB-INF/lang/XT5}'],
            XT5_framework : ['<%= paths.XT5.src %>/framework'],
            XT5_js        : ['<%= paths.XT5.asset_build %>/script'],
            XT5_css_prod  : ['<%= paths.XT5.asset_build %>/style'],
            XT5_css_dev   : ['<%= paths.XT5.asset_src %>/style/index*.+(css|map)',
                             '<%= paths.XT5.asset_src %>/style/less/*.+(css|map)']
        },

        // bower 第三方依赖库安装
        'bower-install-simple' : {
            options : {color : true},
            all     : {options : {production : false}}
        },

        copy : {
            // 复制框架依赖文件
            framework_base_toXT5      : {
                expand : true,
                cwd    : 'framework',
                src    : ['components/**/*.+(js|css|less)'],
                dest   : '<%= paths.XT5.src %>/framework/@project.revision@'
            },
            framework_style_toXT5     : {
                expand : true,
                cwd    : 'framework',
                src    : ['components/**/*.+(css|less)'],
                dest   : '<%= paths.XT5.src %>/framework/@project.revision@'
            },
            framework_cui_toXT5       : {
                expand : true,
                cwd    : 'framework/cui/src',
                src    : ['**/*.+(js|css|less)'],
                dest   : '<%= paths.XT5.src %>/framework/@project.revision@/cui'
            },
            framework_cui_style_toXT5 : {
                expand : true,
                cwd    : 'framework/cui/src',
                src    : ['**/*.+(css|less)'],
                dest   : '<%= paths.XT5.src %>/framework/@project.revision@/cui'
            },
            // 复制页面文件及资源
            framework_resources       : {
                expand : true,
                cwd    : 'web.modules/coremail.framework/src',
                src    : ['**/*'],
                dest   : '<%= paths.build %>'
            },
            XT5_resources             : {
                expand : true,
                cwd    : '<%= paths.XT5.src %>',
                src    : ['XT5/@project.revision@/{style/img,template}/**',
                          'XT5/@project.revision@/style/css/**',                    // style/css目录下的资源需要完整保留
                          '!XT5/@project.revision@/style/index*.+(css|map)',
                          "XT5/*.jsp",
                          "XT5/jsp/**",
                          "WEB-INF/**"],
                dest   : '<%= paths.build %>'
            }
        },

        less : {
            XT5_dev  : {
                options : {
                    strictMath        : false,
                    outputSourceFiles : true,
                    plugins           : lessPlugins,
                    sourceMap         : true,
                    sourceMapURL      : 'index.css.map',
                    sourceMapFilename : '<%= paths.XT5.asset_src %>/style/index.css.map'
                },
                files   : {
                    '<%= paths.XT5.asset_src %>/style/index.css' : '<%= paths.XT5.asset_src %>/style/index.less'
                }
            },
            XT5_prod : {
                options : {
                    compress          : true,
                    strictMath        : false,
                    outputSourceFiles : true,
                    plugins           : lessPlugins,
                    sourceMap         : true,
                    sourceMapURL      : 'index.min.css.map',
                    sourceMapFilename : '<%= paths.XT5.asset_build %>/style/index.min.css.map'
                },
                files   : {
                    '<%= paths.XT5.asset_build %>/style/index.min.css' : '<%= paths.XT5.asset_src %>/style/index.less'
                }
            }
        },

        replace : {
            XT5_css      : {
                src          : [
                    '<%= paths.XT5.asset_src %>/style/index.css',
                    '<%= paths.XT5.asset_build %>/style/index.min.css'
                ],
                overwrite    : true,
                replacements : [
                    {        // 所有 ../img, ../../img 等都替换成 ./img
                        from : /(\.\.\/)+img/g,
                        to   : "./img"
                    }, {     // 修改 ztree 的资源引用的路径
                        from : /\.\/img\/((line_conn|loading|zTreeStandard)\.(gif|png))/g,
                        to   : "./img/ztree/$1"
                    }
                ]
            },
            XT5_finalize : {
                src          : ['<%= paths.build %>/XT5/*.jsp'],
                overwrite    : true,
                replacements : [{
                    from : /\/components\/requirejs\/require/g,
                    to   : '/components/requirejs/require.min'
                }, {
                    from : /\/script\/main/g,
                    to   : "/script/main.min"
                }, {
                    from : /\/style\/index.css/g,
                    to   : "/style\/index.min.css"
                }]
            }
        },

        csslint : {
            XT5 : {
                options : {
                    csslintrc : '<%= paths.XT5.asset_src %>/style/.csslintrc'
                },
                src     : ['<%= paths.XT5.asset_src %>/style/**/*.css']
            }
        },

        // 合并 js 文件
        requirejs : {
            XT5_compileJs : {
                options : {
                    mainConfigFile          : '<%= paths.XT5.asset_src %>/script/main.js',
                    name                    : 'main',
                    include                 : ['app'],
                    out                     : '<%= paths.XT5.asset_build %>/script/main.js',
                    optimize                : 'none',
                    preserveLicenseComments : true
                }
            },
            XT5_uglifyJs  : {
                options : {
                    mainConfigFile          : '<%= paths.XT5.asset_src %>/script/main.js',
                    name                    : 'main',
                    include                 : ['app'],
                    out                     : '<%= paths.XT5.asset_build %>/script/main.min.js',
                    optimize                : 'uglify2',
                    preserveLicenseComments : false,
                    generateSourceMaps      : true
                }
            }
        },

        // 单独压缩某些JS文件
        uglify : {
            options   : {sourceMap : true},
            requirejs : {
                files : {
                    '<%= paths.components_build %>/requirejs/require.min.js' : //
                            'framework/components/requirejs/require.js'
                }
            },
            cui       : {
                files : {
                    '<%= paths.components_build %>/../cui/util/IE8-hack.js' : //
                            'framework/cui/src/util/IE8-hack.js'
                }
            }
        },

        watch : {
            setup   : {
                files   : [],
                tasks   : ['bower'],
                options : {atBegin : true, spawn : false}
            },
            XT5_css : {
                files   : ['<%= paths.XT5.asset_src %>/style/**/*.less'],
                tasks   : ['XT5_dev:css'],
                options : {atBegin : true, spawn : false}
            }
        }
    });


    // load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
    require('./src/node/grunt-utils').registerTasks({
        'default' : ['clean', 'XT5_build', 'prod_final'],
        'dev'     : ['XT5_dev'],

        bower     : ['bower-install-simple:all'],
        framework : {
            XT5_clean      : ['clean:XT5_framework'],
            XT5_copy       : ['copy:framework_base_toXT5', 'copy:framework_cui_toXT5'],
            XT5_copy_style : ['copy:framework_style_toXT5', 'copy:framework_cui_style_toXT5']
        },

        XT5_clean : ['clean:XT5', 'clean:XT5_css_dev'],

        XT5_dev : {
            clean : ['XT5_clean'],
            bower : ['bower'],
            js    : ['clean:XT5_js', 'framework:XT5_copy', 'requirejs:XT5_compileJs', 'framework:XT5_clean'],
            css   : ['clean:XT5_css_dev', 'framework:XT5_copy_style', 'less:XT5_dev', 'replace:XT5_css', 'framework:XT5_clean', 'csslint:XT5']
        },

        XT5_build : {
            clean : ['XT5_clean'],
            bower : ['bower'],
            js    : ['clean:XT5_js', 'framework:XT5_copy', 'requirejs:XT5_uglifyJs', 'uglify', 'framework:XT5_clean'],
            css   : ['clean:XT5_css_prod', 'framework:XT5_copy', 'less:XT5_prod', 'replace:XT5_css', 'framework:XT5_clean']
        },

        prod_final : {
            // 使用 ISO-8859 编码以确保不会因为 node js 的转码错误而产生锟斤拷
            prepare : function () { grunt.file.defaultEncoding = "ISO-8859-1"; },
            copy    : ['copy:framework_resources', 'copy:XT5_resources'],
            replace : ['replace:XT5_finalize']
        }
    });
};
